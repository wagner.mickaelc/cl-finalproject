from main import count_colors
import json
from unittest import TestCase


class TryTesting(TestCase):
    def test_count_colors(self):
        expectedDict: dict = {"H": 13, "S": 13, "D": 13, "C": 13}
        with open('test/cards.json') as f:
            data = json.load(f)
        assert count_colors(data["cards"]) == expectedDict
