from main import create_deck, draw_cards, count_colors

if __name__ == "__main__":
    deck_id = create_deck()
    r = draw_cards(10, deck_id)
    count = count_colors(r)
    print(count)
