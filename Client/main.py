import requests
import json


def create_deck():
    r = requests.get("http://localhost:8000/creer-un-deck/")
    output = r.json()
    if output["success"]:
        return(output["deck_id"])


def draw_cards(nombre_cartes, deck_id):
    r = requests.post(
        "http://localhost:8000/cartes/"+str(nombre_cartes),
        data=json.dumps({"deck_id": deck_id})
    )
    if "cards" in r.json().keys():
        return(r.json()["cards"])
    else:
        return([])


def count_colors(cards):
    output = {"H": 0, "S": 0, "D": 0, "C": 0}
    for card in cards:
        if card["suit"] == "CLUBS":
            output["C"] += 1
        elif card["suit"] == "DIAMONDS":
            output["D"] += 1
        elif card["suit"] == "HEARTS":
            output["H"] += 1
        elif card["suit"] == "SPADES":
            output["S"] += 1
    return(output)
