import uvicorn
from fastapi import FastAPI
import requests
from pydantic import BaseModel

from config import HOST_PORT
from config import HOST_NAME


class Item(BaseModel):
    deck_id: str


app = FastAPI()


@app.get("/creer-un-deck/")
def createDeck():
    r = requests.get(
        "https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1"
    )
    print(r.json())
    return r.json()


@app.post("/cartes/{nombre_cartes}")
def drawCards(nombre_cartes: int, item: Item):
    print("Ok !")
    r = requests.get(
        "https://deckofcardsapi.com/api/deck/" + item.deck_id + "/draw/?count="
        + str(nombre_cartes)
    )
    return r.json()


if __name__ == "__main__":
    uvicorn.run(app, host=HOST_NAME, port=HOST_PORT)
