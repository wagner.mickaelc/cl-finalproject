# Readme

## But
Ce projet à pour but de mettre en place un webservice utilisant l'API `https://deckofcardsapi.com/` afin de créer un jeu de carte, le battre et tirer des cartes.

## Récuperation du projet
```
git clone git@gitlab.com:wagner.mickaelc/cl-finalproject.git
```
ou
```
git clone https://gitlab.com/wagner.mickaelc/cl-finalproject.git
```
puis
```
cd cl-finalproject
```

## Initialisation du projet

### Configuration
Par défaut, l'adresse ainsi que le port utilisés pour le serveur sera locahost:8000.<br>
Si vous souhaitez changer ces valeurs, veuillez modiifer les valeurs de port ou d'adresse qui se se situent dans dossier WebService/main.py
### Serveur
```
cd WebService
pip install -r requirements.txt
```

### Client
```
cd Client
pip install -r requirements.txt
```
## Quick start
Un scenario vous est proposé afin de faire fonctionner l'application.
### Serveur
```
cd WebService
python main.py
```
### Scenario
```
cd Client
python scenario.py
```
## Lancement des tests unitaires
```
cd Client
pytest
```

## Intégration continue
Il est a noter que l'intégration continue est actuellement configurée, si vous utilisez gitlab, de manière assez stricte car elle impose de respecter le linting de flake8 à la lettre. Il est conseillé de désactiver la partie linting au besoin ou de modifier la configuration de celui-ci.


## Cas limites

### API Utilisée
Notre application se base sur une API, qui aux premiers abords semblait plutôt fiable. Cependant, il n'est pas exclu que cette API ne soit pas accessible ou que les roots soient modifiés. Dans ces deux cas là, notre application serait inutilisable.

### Plus aucune carte dans le paquet
Lorsque nous tirons des cartes, il se peut que le paquet sélectionner ne possède pas suffisament de carte :
- Ainsi, notre WebService renverra la réponse de l'API utilisée, à savoir un success à false, les cartes qui ont pu être tirées ainsi qu'un message d'erreur.
- Notre client quand à lui fera le choix de récupérer la liste des cartes tirés si il y'en a ou renverra une liste vide pour toute erreur survenue lors de la requête à l'API. Ainsi, on pourrait immaginer un scénario qui compte le nombre réellement tirés et qui recrée finalement un deck pour tirer les cartes manquants le cas échéant.

## Hors Sujet
En ce qui concerne la suppression du dossier .vscode, j'ai conscience que l'ajouté du gitignore ne l'efface pas de l'historique. Mais ce n'est pas non plus un fichier qui ne doit pas être versionné donc celui qui voudrait s'amuser à remonter le temps sera peut être content de tomber sur une configuration du linting pour passer cette intégration continue !